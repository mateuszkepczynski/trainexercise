import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * DODATKOWE: zamień implementację headquarters na JFrame, lub dodaj w niej implementację tak aby w tej klasie pojawilo sie okno
 * (dodaj interfejs GUI) aby na okienku (headquarters) wyswietalala sie lista wszystkich pociagow. (podobnie jak pralki)
 * <p>
 * Created by amen on 9/13/17.s
 */
public class Headquarters implements Observer {

    public final static Boolean DIRECTION_WEJHEROWO = false;// wejherowo to stacja (stations.length -1)
    public final static Boolean DIRECTION_GDANSK = true;    // gdansk to stacja 0

    // ciuf ciuf
    private ExecutorService pociongi = Executors.newFixedThreadPool(10);

    private TrainSchedule scheduleFromGdansk;
    private TrainSchedule scheduleFromWejherowo;

    private List<Station> stationList;

    public Headquarters() {
        this.scheduleFromGdansk = new TrainSchedule(new LinkedList<TrainScheduleRecord>(
                Arrays.asList(new TrainScheduleRecord(1000, false),
                        new TrainScheduleRecord(1000, false),
                        new TrainScheduleRecord(1000, false),
                        new TrainScheduleRecord(1000, false),
                        new TrainScheduleRecord(1000, false))));

        this.scheduleFromWejherowo = new TrainSchedule(new LinkedList<TrainScheduleRecord>(
                Arrays.asList(new TrainScheduleRecord(1000, false),
                        new TrainScheduleRecord(1000, false),
                        new TrainScheduleRecord(1000, false),
                        new TrainScheduleRecord(1000, false),
                        new TrainScheduleRecord(1000, false))));

        // UWAGA! TRZEBA DODAĆ STACJE
        stationList = new LinkedList<Station>(
                Arrays.asList(new Station(0, scheduleFromGdansk, scheduleFromWejherowo),
                        new Station(1, scheduleFromGdansk, scheduleFromWejherowo),
                        new Station(2, scheduleFromGdansk, scheduleFromWejherowo),
                        new Station(3, scheduleFromGdansk, scheduleFromWejherowo),
                        new Station(4, scheduleFromGdansk, scheduleFromWejherowo),
                        new Station(5, scheduleFromGdansk, scheduleFromWejherowo)));


        // dodaj stacje aby obserwowały centralę lub sam/a zaimplementuj powiadamianie w metodzie update.
    }

    public void dispatchTrainFromGdansk() {
        Train t1 = new Train(0, DIRECTION_WEJHEROWO, scheduleFromGdansk);
        t1.addObserver(this);
        pociongi.submit(t1);
    }


    public void dispatchTrainFromWejherowo() {
        Train t2 = new Train(stationList.size() - 1, DIRECTION_GDANSK, scheduleFromWejherowo);
        t2.addObserver(this);
        pociongi.submit(t2);
    }

    public void update(Observable o, Object arg) {
        if (o instanceof Train) {
            Train t = (Train) o;
            if (t.isCanceled()) {
                System.out.println("Pociąg " + t.getTrain_id() + " został anulowany");
            } else {
                if (t.getDirection()==DIRECTION_WEJHEROWO) {
                    for (int i = t.getStationId(); i < stationList.size(); i++) {
                        stationList.get(i).update(t, arg);
                    }
                } else {
                    for (int i = t.getStationId(); i >= 0; i--) {
                        stationList.get(i).update(t, arg);
                    }
                }
            }
        }
        // jest informowany o tym że jakiś pociąg dojechał do stacji i informuje o tym stacje.
        // to jakie stacje muszą być poinformowane wybieracie WY

    }

    public void printTrainInfoOf(int id) {
        stationList.get(id).print();

        // wypisz tablice pociagow stacji z identyfikatorem id;
    }

}
