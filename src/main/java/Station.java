import java.util.*;

/**
 * Reprezentuje pojedyncza stacje kolejowa.
 * <p>
 * DODATKOWE: zamień implementację station na JPanel (dodaj interfejs GUI) aby na okienku (headquarters)
 * wyswietalala sie lista wszystkich pociagow.
 * Created by amen on 9/13/17.
 */
public class Station implements Observer {

    private int id;
    private TrainSchedule fromGdansk;   //grafik w jedna strone
    private TrainSchedule fromWejherowo;//grafik w druga strone

    private PriorityQueue<TrainInfo> listOfTrainsFromGdansk;
    private PriorityQueue<TrainInfo> listOfTrainsFromWladyslawowo;


    public Station(int id, TrainSchedule fromGdansk, TrainSchedule fromWejherowo) {
        this.id = id;
        this.fromGdansk = fromGdansk;
        this.fromWejherowo = fromWejherowo;

        this.listOfTrainsFromGdansk = new PriorityQueue<TrainInfo>();
        this.listOfTrainsFromWladyslawowo = new PriorityQueue<TrainInfo>();
    }

    public void update(Observable o, Object arg) {
        if (o instanceof Train) {
            Train t = (Train) o;
            System.out.println("Stacja o id " + id + " zostaje poinformowana o pociągu " + t.getTrain_id());
            int obecna_stacja = t.getStationId();
            int nasza_stacja = id;
            int id_pociag = t.getTrain_id();
            if (obecna_stacja == nasza_stacja) {

            } else {


                if (t.getDirection() == Headquarters.DIRECTION_WEJHEROWO) {
                    if(obecna_stacja==0){
                        long timeOfArrival = countTimeOfArrival(obecna_stacja,nasza_stacja,fromGdansk);
                        listOfTrainsFromGdansk.add(new TrainInfo(timeOfArrival,id_pociag));
                    }else{

                    }
                } else {
                    if(obecna_stacja==fromWejherowo.getSchedule().size()){
                        long timeOfArrival = countTimeOfArrival(obecna_stacja,nasza_stacja,fromWejherowo);
                        listOfTrainsFromWladyslawowo.add(new TrainInfo(timeOfArrival,id_pociag));
                    }

                }
            } 
        }
        // lista wyświetla pociągi zgodnie z tylko tym, co ruszyło już ze stacji.
        // tutaj musi pojawić się "skomplikowana" logika oceny czasu dojazdu pociągu.
        // UWAGA! stacja nie wie o tym że pociąg nie zatrzymuje się na stacjach (innych niż ona sama)
        // UWAGA! (nie ma tego brać pod uwagę przy obliczaniu czasu dojazdu) więc pociagi beda zmienialy czasy dojazdow
        // Spróbuj użyć komparatora (liste listOfTrainsFromGdansk oraz listOfTrainsFromWladyslawowo zamien na priorityqueue)
    }

    private long countTimeOfArrival(int stacja_pociągu, int nasza_stacja, TrainSchedule rozkład) {
        long wynik = 0;
        for (int i = 0; i <nasza_stacja ; i++) {
            wynik+=rozkład.getSchedule().get(i).getTime();
        }
        return wynik;
    }
public void print(){
        Iterator<TrainInfo> infos = listOfTrainsFromGdansk.iterator();
    while (infos.hasNext()){
        System.out.println(infos.next());
    }
}
    public class TrainInfoComparator implements Comparator<TrainInfo> {
        public int compare(TrainInfo o1, TrainInfo o2) {
            return o1.getEstimatedTimeOfArrival() > o2.getEstimatedTimeOfArrival() ? -1 : 1;
        }
    }
}
