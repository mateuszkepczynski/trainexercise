import java.util.Observable;
import java.util.Observer;

/**
 * Reprezentuje poruszający się pociąg.
 * Created by amen on 9/13/17.
 */
public class Train extends Observable implements Runnable {
    private static int train_counter = 0;
    private int train_id = train_counter++;
    private int stationId;
    private boolean direction; //true is increment, false is decrement;
    private boolean canceled = false;
    private TrainSchedule schedule;

    public Train(int stationId, boolean direction, TrainSchedule schedule) {
        this.stationId = stationId;
        this.direction = direction;
        this.schedule = schedule;
    }

    public void run() {
        System.out.println("Pociąg rusza");
        int counter = 1;
        setChanged();
        notifyObservers();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Wyjeżdżam ze stacji  "+stationId);
        for (TrainScheduleRecord trainScheduleRecord : schedule.getSchedule()) {

            if (!canceled) {
                try {
                    Thread.sleep(trainScheduleRecord.getTime());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (!direction) {
                    stationId++;
                } else {
                    stationId--;
                }
                setChanged();
                notifyObservers(stationId);
                System.out.println("Wjechałem na stację " + stationId);
                if (!trainScheduleRecord.isSkip()) {
                    System.out.println("Pociąg zatrzymuje się na stacji " + stationId);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
                if (counter != schedule.getSchedule().size()) {
                    System.out.println("Wyjeżdżam ze stacji " + stationId);
                }


            } else {
                setChanged();
                notifyObservers(train_id);
                System.out.println("Pociąg " + this.getTrain_id() + " zjeżdża na bocznicę >>>>>");
                break;
            }
counter++;
        }
        // każdy pociąg porusza się po linii
        // każdy pociąg jeśli zatrzymuje sie na stacji, to czeka na niej dokładnie 1 sek.
        // każdy pociąg może być anulowany
        // każdy pociąg informuje headquarters o tym że dojechał na stację, nawet jeśli nie zatrzymuje się na niej (nie czeka 1s.)
    }

    public int getStationId() {
        return stationId;
    }

    public void cancelTrain() {
        canceled = true;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public int getTrain_id() {
        return train_id;
    }

    public boolean getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return "Train{" +
                "train_id=" + train_id +
                ", stationId=" + stationId +
                ", direction=" + direction +
                ", canceled=" + canceled +
                ", schedule=" + schedule +
                '}';
    }
}
